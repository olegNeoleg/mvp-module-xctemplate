# README #

This repo contains .xctemplate that generates MVP module with given name  

### Set up ###

* Navigate to where Xcode stores its templates. Path: `~/Library/Developer/Xcode/`
* From there create folders with path `Templates/File\ Templates/Custom`
* Copy `MVP Module.xctemplate` to `Custom` folder
* Now template should be avaliable
