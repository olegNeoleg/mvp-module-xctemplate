//___FILEHEADER___

import UIKit

protocol ___VARIABLE_productName___AssemblyProtocol {
    func build() -> UIViewController
}

class ___VARIABLE_productName___Assembly: ___VARIABLE_productName___AssemblyProtocol {
    func build() -> UIViewController {
        let view = ___VARIABLE_productName___ViewController()
        let router = ___VARIABLE_productName___Router(view: view)
        let presenter = ___VARIABLE_productName___Presenter(view: view, router: router)
        view.presenter = presenter
        
        return view
    }
}
