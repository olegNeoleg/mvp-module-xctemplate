//___FILEHEADER___

import UIKit

protocol ___VARIABLE_productName___RouterProtocol {
    init(view: UIViewController)
}

class ___VARIABLE_productName___Router: ___VARIABLE_productName___RouterProtocol {
    private weak var view: UIViewController?
    
    required init(view: UIViewController) {
        self.view = view
    }
}
